\documentclass[runningheads]{llncs}
%
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage[linesnumbered,ruled]{algorithm2e}
\usepackage{enumitem}
% Used for displaying a sample figure. If possible, figure files should
% be included in EPS format.
%
% If you use the hyperref package, please uncomment the following line
% to display URLs in blue roman font according to Springer's eBook style:
% \renewcommand\UrlFont{\color{blue}\rmfamily}
%\SetKwRepeat{Do}{do}{while}
\begin{document}
%
\title{Comité de suivi individuel de thèse}
\subtitle{Report}
\author{PhD student: Antoine Barczewski\\Supervisor: Jan Ramon}
\institute{Université de Lille - Inria}
\maketitle


% TODO: add bibliography (through zotero)
% TODO: ask about the name of the proof for the gradient

\section{Thesis description and objectives}
\subsection{Context}
Over the last years, there has been an increasing interest in privacy-preserving machine learning, i.e., learning while protecting the sensitive underlying training data.
Differential privacy \cite{Dwork2014a} has become the gold standard to measure the privacy of an algorithm.  For a wide range of machine learning strategies, versions have been proposed which output models with an appropriate amount of noise to satisfy differential privacy.  We are interested in leveraging differential privacy techniques to improve machine learning key metrics or core tools.

\subsection{Objectives}
In this PhD, we focus on developping privacy-preserving machine learning
algorithms which allow for transparency to both developers using these
algorithms and end-users wanting to understand how their data is handled.
We start from an analysis of the information flows in a system,
and research algorithms to reason about what measures (such as noise or encryption)
are needed in each step of a complex process (including both learning and
inference steps) to globally satisfy a given set of privacy requirements
while achieving good performance.  Moreover, we research strategies to
explain the measures taken and the guarantees that can be proven to end-users
in an understandable way.  We apply the developed theory and algorithms
in a real-world application such as one can find in the medical domain.

% internal ref in close brackets
To achieve these objectives, we have started contributing on the following. We propose strategies to compute occupancy modeling patient flows as Finite State Machine (section \ref{Finite State Machine}), differentially private empirical distribution functions (section \ref{dp_ecdf}) and differentially private gradient descent with empirically estimated sensitivity (section \ref{empirical_optimum}).  While the first two benefits from research on differential privacy under continual observations, the last one explores an empirical way of callibrating the added noise. All this topics can find applications in the medical fields as they bring utility without undermining privacy.


\section{Overview of relevant existing work}

Differential privacy \cite{Dwork2014a} has become a standard to provide mathematical guarantees to any individual in a dataset. More specifically, a mechanism is differentially private if the log ratio of the outcomes from an algorithm applied to two adjacent datasets is bounded by a privacy parameter $\epsilon$. We say that two datasets are adjacent if they differ by only one example. Popular differentially private mechanisms consist in adding Laplacian or Gaussian noise to the input or to the output of an algorithm.
% \begin{definition}[differential privacy]
%   Let $\epsilon>0$.  Let $\mathcal{A}$ be an algorithm taking as input datasets from $\mathcal{X}^*$.  Two datasets $X^{(1)},X^{(2)}\in \mathcal{X}^*$ are adjacent if they differ in only one element.   The algorithm $\mathcal{A}$ is $\epsilon$-differentially private ($\epsilon$-DP) if for every pair of adjacent datasets $X^{(1)}$ and $X^{(2)}$, and every subset $S$ of possible outputs of $\mathcal{A}$, $P(\mathcal{A}(X^{(1)})\subseteq S) \le e^\epsilon P(\mathcal{A}(X^{(2)})\subseteq S)$.
% \end{definition}


% related work from icml
% - composition naive: ([DMNS06, DKM+06a, DL09, dwork_boosting_2010]). For any ε > 0 and δ ∈ [0, 1], the class
% of (ε, δ)-differentially private mechanisms satisfy (kε, kδ)-differential privacy under k-fold adaptive
% composition.
% - composition kfold: However, if we allow for a slightly larger value of δ, then Dwork et al. showed in [dwork_boosting_2010] that one
% can gain a significantly higher privacy guarantee in terms of ε.
% - blr: The blum_learning_nodate mechanism uses the exponential mechanism to sample a synthetic database privately. We
% have already seen that the set of m-row databases contains at least one “good’ synthetic database
% that will preserve the answers to all of the queries in Q. The analysis of the blum_learning_nodate mechanism
% will show that the output of the exponential mechanism is strongly concentrated around the set of
% “good” synthetic databases.
% - specific to counter: mechanisms from song
% - continual observations
% - application to fsm and empirical cumulative distribution function

\subsubsection{Differential privacy with repeated querying.}
Naive approach consists in adding independant noise \cite{dwork_calibrating_nodate} at each query but, as the privacy budget grows linearly with the number of queries, this mechanism loses quickly its utility. Building adaptively the noise from the one added in the previous query, as described in the 1 k-Fold Adaptive Composition theorem \cite{dwork_boosting_2010}, can achieve significantly higher privacy guarantees. Another way to correlate the noise is to create a synthetic database that preserves the results of all queries \cite{blum_learning_nodate}. Although these techniques are the best known composition techniques so far \cite{dwork_boosting_2010,blocki_johnson-lindenstrauss_2012,hardt_beyond_2013}, they fail to achieve the best accuracy, under differential privacy, when we know in advance the query that will be repeated. Private counters, for instance, can benefit from the binary mechanism developed by Dwork \cite{dwork_differential_2010} which only requires a logarithmic number of added noise in the number of queries to preserve privacy.

\subsubsection{Secured aggregation.}
Many strategies have been proposed to compute aggregation like averages with higher or lower security and privacy guarantees.
The simplest but least secure is the classical trusted curator setting where all input is sent to a trusted party which makes the average and sends it back.
Strategies such as \cite{Shi2011,Bonawitz2017a,ChanSS12} focus on securely computing an average without disclosing the data to any party, assuming a honest-but-curious setting, i.e., parties are assumed to follow the protocol honestly even if they are curious and may try to infer information from what they observe.
The strategy proposed in \cite{Dwork2006ourselves} aim at better verifiability, but induces a cost quadratic in the number of parties.  The algorithms in \cite{Jayaraman2018,Sabater2021sample} integrate more strongly noise addition in the secure aggregation step, but relies on the additional assumption that two servers don't collude.
Recently, the shuffle model of privacy \cite{Cheu2019,amp_shuffling,Hartmann2019,Balle2020,Ghazi2020ICML} has been studied, where inputs are passed via a trusted/secure shuffler that obfuscates the source of the messages, leading to an alternative trust model.  It is also possible to distribute trust over the participating parties rather than relying on a limited number of servers for secret keeping \cite{Sabater2021accurate}.

\subsubsection{Differentially Private Empirical Risk Minimization.}
Differentially Private Empirical Risk Minimization (DP-ERM) has first been studied by adding noise to the result of the optimization program along its objectives \cite{chaudhuri_differentially_nodate}. To provide a gradient descent already private rather than only its result, noise terms are added at each gradient update \cite{bassily_differentially_2014} (DP-SGD). Faster convergence of the latter have been developed \cite{wang_differentially_2017} building up differential privacy on the SVRG algorithm \cite{johnson_accelerating_2013}. Although DP-SGD has turned out to be the standard approach, competitive results have been achieved when challenging the protocol of the gradient descent towards ERM, such as differentially private coordinate-wise updates \cite{mangold_differentially_2022}.


\section{Thesis progress}

\subsection{Repeated querying of hospital unit occupancy modeling patient flows as Finite State Machine} \label{Finite State Machine}
\subsubsection{Motivations.}
Suppose an hospital unit needs to count patients suffering from a specific disease on a regular basis in each of its services. We define each patient as a  Finite State Machine (FSM). The patient receives inputs from the hospital staff so he or she changes state by changing service to eventually get back to the initial state - the patient is outside of the hospital. Let $Q$ be the set of all states corresponding to all services of an hospital unit. Let $\sigma_{in,q}$ and $\sigma_{out,q}$ be respectively the incoming and outgoing streams of patients in state $q \in Q$. For each state $q \in Q$, we define $T_{max}$ be the maximum length of stay in the hospital. What is the best strategy to implement such counters bearing in mind an adversary has access to reports on the number of patients from all services?

Besides the practical application, our motivation is to leverage the underlying structure of the data - streams of finite state machines - to be able to answer privately potentially infinite querying.

\subsubsection{Contributions.}
According to \cite{dwork_differential_2010}, only a logarithmic amount of noise terms in the number of evaluation is enough to build a private counter, let's call this method a binary mechanism. Bearing in mind that a patient cannot stay longer than $T_{max}$, we need to guarantee privacy for this period and thus store at most $\lceil\log_2(T_{max})\rceil$ noise terms. Each period of $2^{\lceil\log_2(T_{max})\rceil}$ time steps, we flush outdated noise terms and start a new binary mechanism. \newline
In summary, our contributions are:
\begin{itemize}
  \item We build a differentially private counter on a process that runs for a long (possibly infinite) time and is queried after every step.
  \item For this we use Finite State Machines.
  \item We prove differential privacy guarantees for every state of the Finite State Machine.
  \item We measure empirically these guarantees with synthetic data.
\end{itemize}

\subsubsection{Future work.}
Generally speaking, we need to get our modelling closer to reality. To do so, first we have to generalize our basic assumptions.  We have defined $T_{max}$ as the maximum length of stay in the hospital as a constant, but it will be more realistic to have it follow a poisson distribution. Second, our model has to become robust enough to take into account edge cases within the patient flow. For instance, patients should be able to move from one unit to the other within the same day etc. Finally, we need to broaden the applications beyond counting to more advanced statistical queries.

\subsection{Differentially private empirical cumulative distribution functions} \label{dp_ecdf}
\subsubsection{Motivations.} Amal Mawass first initiated this study, then I contributed to it when we tried to publish at ICML and finally, she completely handed it over to me when she unfortunately ended her PhD. Although the paper has not been accepted, feedbacks are positive enough to confort us in trying to publish it.

We propose strategies to compute differentially private empirical cumulative distribution functions (ECDF). ECDF play an important role in statistics and machine learning, e.g., in tail bounds and in statistics based on rankings. Often an ECDF is an intermediate result, however knowledge of the complete function is in many cases an asset of its own value. Publishing a complete ECDF requires more noise than publishing a single aggregated value, but is also much more informative.

\subsubsection{Contributions.}
% As in the previous topic, we have based our methodology on \cite{dwork_differential_2010}, to wit the binary mechanism.
In summary, our contributions are:
\begin{itemize}
  \item We propose a strategy for generating differentially privately a complete ECDF.  We prove that the resulting curve is $\epsilon$-differentially private.
  \item In the process of doing so, we make a minor constant-factor improvement over differential privacy guarantees for continually observed statistics as shown in \cite{DBLP:journals/corr/abs-2103-16787}.
  \item As a differentially private version of a non-decreasing function is not necessarily non-decreasing itself, we propose a strategy to smooth differentially private functions.  This makes the function non-decreasing again, and in some cases may even reduce the error induced by the differential privacy noise.
  \item We discuss strategies for efficient implementations.  In particular, we propose both a generic algorithm which can start from any secure aggregation operator and inherit its security/privacy model and a specific strategy strategy based on secret sharing which has asymptotically better complexity.
  \item We present in more detail two applications.  First, we discuss how to make $\epsilon$-differentially private ROC curves using our technique.  Second, we present a differentially private Hosmer-Lemeshow statistic.
  \item We illustrate our techniques with a number of experiments.
\end{itemize}
\subsubsection{Future work.}
There are several potentially interesting lines of future work.  Among others it would be interesting to elaborate more applications of ECDF, to develop more efficient algorithms to securely compute private ECDF and get a better understanding of the various statistical processes affecting the error DP noise induces.

\subsection{Differentially private gradient descent with empirically estimated sensitivity} \label{empirical_optimum}
\subsubsection{Motivations.}
Trained machine learning models can leak sensitive information about their underlying training dataset. To qualify this risk, differentially private versions of Empirical Risk Minimization (ERM) have arised to achieve privacy guarantee without underming overall performances. The most popular approach is the DP-SGD \cite{bassily_differentially_2014} which basically adds random pertubations at each update of the gradient. However, these techniques have poor performances  when reasonnable guarantees of differential privacy is achieved. We propose a novel approach to better estimate the scale of the pertubations added which eventually brings better performance and reduces the variance of the resulting optimum.

\subsubsection{Contributions.}
The DP-SGD scales the noise terms added at each update to an upper bound of the actual sensitivity. Based on that, we empirically estimate the latter to eventually lower down the scale of the added noise terms. To do so, we append one synthetic vector to the training dataset which activates only one feature and we store the corresponding result from the gradient descent. We do that as many times as there are features, each time activating a different feature so we end up with a set of optima from which we compute the overall sensitivity. \newline
In summary, our contributions are:
\begin{itemize}
  \item We build a python module to empirically estimate the sensitivity of the optimization algorithm.
  \item We develop a DP logistic regression model based on this estimated sensitivity.
  \item We compare variances in coefficients of the optimum and performances between our module and the tensorflow DP-logistic regression on three datasets and over several privacy budgets.
  \item We prove empirically our method (to be completed).
\end{itemize}
\subsubsection{Future work.}
Future work is twofold, building the proof and improving the implementation. Regarding the proof, we have already started to empirically measure how the effect of one feature is separated from the effect of the other features in the space of the optima. Without this we cannot compute the sensitivity as the maximum range for each coefficient of the optimum. Beyond the empirical proof, we will need to find theoretical argument that can support this methodology. As for the implementation, we have only focused on quick descents that require less than two updates so the noise could be added directly to the resulting optimum. We will change the protocol so each update of the descent, when the descent requires more than one update, is differentially private. Again, we will assess the performance of the methodology against state of the art implementations both in terms of machine learning performances and of resulting variance of the coefficients.

\section{Roadmap}
%promissing but not to concrete
% explore future work
% explore application of compounded dp : dp gd
% explore continuous process
%
% find applications in the medical domain
% implement package

The research will go on in five separate threads. The first one is to look into the future lines of work already sketched above. To sum them up, we will need to be able to generalize on our assumptions, to take into acount edge cases and to explore more applications in the medical domain to assess our models. The second thread consists in finding new areas of research that can leverage techniques on privacy of continuous process. The third one will be on how to optimize the privacy mechanisms designed above. The forth thread aims at giving the right information to the end-user so he or she can fully trust our models. Finally, the last thread is about the implementation of the resulting models into ready-to-use Python packages. We will try, as much as possible, to make these packages fit the currentlty in the making library built by the Magnet team.

Ongoing work is already detailed in the previous section under "Future work" tag. So, below, we will only go through the remaining four threads.

\subsection{Privacy of continuous processes}
We have made progress on methods to effectively disclose information on continuous process with privacy guarantees. So far, we have focused our work on discretionized processes with counting tasks, but we need to extend the research to fully continuous processes. Indeed, the auto-correlation of a signal can be used to shape the noise required to display a differentially private version of this signal \cite{wang_cts-dp_2017}. Direct applications can be found for this methodology as well as it opens new perspectives for existing methods linked to differential privacy. For instance, the gradient descent update can benefit from a lower amount of noise, in its differentially private version, if the noise sequence is correlated. We have already started to explore the latter, but we have not yet tested the optimal settings where this method brings results.

\subsection{Privacy plan optimization}
Once the privacy mechanism is defined, one needs to find the optimal settings to balance privacy and performances. Combining building blocks of the privacy mechanism together can ensure privacy guarantees but will fail to maximize the overall utility. An alternative way consists in shifting to a top-down approach where privacy requirements are as many settings as a unified optimization program requires. This novel approach is detailed in \cite{koprinska_interpretable_2020} which has been produced by a Magnet PhD student. We will take over from her in order to explore tractability, scalability and new applications such as the hospital modeling problem we have set above.

\subsection{End-user transparency}
The privacy plan optimization results in both a fitted mechanism and the corresponding optimal set of parameters. The latter holds rich information about the "decisions" made by the optimization program among which there are the decisions linked to the privacy settings. Hence interpretability can be given to both the developper when he or she builds his or her model and to the end-user who can understand where and how its privacy leaks. Interpretability in the privacy field is fairly new and hence requires exploring and testing but it seems a necessary steps when it comes to the user adoption of a model, especially when this model claims preserving privacy.

\subsection{Implementation}
The Magnet team is currentlty developping an open source project, called Tailed, that gathers differential privacy and security techniques to enable the use of machine learning models in a privacy-preserving fashion. While engineers are the main contributors to the project, PhD students can also support the effort by implementing their models when relevant. For instance, the private ECDF is definitely a good candidate for Tailed as it enables differentially private ROC curves and other necessary machine learning tools. Although we have already built a module dedicated to private ECDF, we need to adapt it so it can work in a decentralized set up and to develop a version built upon function secret sharing.

\subsection{Estimated timeline}
The implementation is a thread that will span across the whole PhD, while the first two - ongoing work and new areas of research - will roughly follow this timeline:
\begin{itemize}
% ongoing research rather than existing
  \item 07/2022 -- 11/2022 : Ongoing research - we are here
  \item 12/2022 -- 03/2023 : Privacy of continuous processes
  \item 04/2023 -- 06/2023 : Application in medical domains
  \item 07/2023 -- 11/2023 : Privacy plan optimization
  \item 12/2023 -- 05/2024 : End-user transparency
  \item 06/2024 -- 09/2024 : Application and validation
  \item 10/2024 -- 12/2024 : writing of PhD dissertation
  \item 01/2025 -- 03/2025 : revisions and defense
\end{itemize}

\section{Doctoral training}
I plan to get credits through three different channels: doctoral school, training "hors catalogue" and  conference / summer school. Overall, I plan to earn the required amount of credits by the end of my second year, to wit April 2024.

\subsection{Doctoral school}
I'm currently enrolled in the following classes:
\begin{itemize}
  \item Writing Successful Scientific Papers / Scientific Writing
  \item Propriété intellectuelle au service des doctorants (tronc commun)
  \item Histoire des Mathématiques : l'émergence des mathématiques appliquées (1830-1930)
\end{itemize}
They are all scheduled between September and October.

On top of that, as soon as they are available in ADUM, I would like to attend the following courses:
\begin{itemize}
  \item Retrieving and monitoring scientific information and literature BSL, ENGSYS, MADIS, SMRE
  \item Atelier Zotero - Niveau Expert
  \item Composition efficace du mémoire de thèse - Avancé
\end{itemize}

\subsection{Training "hors catalogue"}
I have already validated the FUN MOOC "Ethics and STICs" ($8$ credits) and I am currently enrolled in the Udacity MOOC "Secure and private AI" ($10$ credits). While the former is generic to every PhD students, the latter provides great material specific to my areas of research.

% add dates of eta within training plan
\subsection{Conference / Summer school}
Unfortunately, I cannot make it to 2022 summer school in privacy-preserving machine learning but I will definitely attend the 2023 edition. More precisely, I would like to take part of either one of the following:
\begin{itemize}
  \item Inria-DFKI European Summer School on AI, on Trusted AI and Sustainable AI
  \item PPML School in Copenhagen
  \item BIU winter school, on cryptography
\end{itemize}
The first two summer schools would allow me to talk and meet researchers in the domain of my thesis, while the last one would let me learn more on cryptography which is a bit of blind spot for me.

I hope that one my work will get publish in a conference, which also give me the opportunity to present it in front of an audience.

\section{Professional project}
I would like to leverage the knowledge gathered during my PhD to have impact in the medical field. Whether it be in the public (e.g. Inserm, AP-HP) or in the private sector (e.g. Owkin), I would like to contribute to the effort of bringing machine learing tools to the "Assistance Publique" with a true privacy-preserving approach.


\bibliographystyle{IEEEtran}
\bibliography{biblio}
\end{document}
