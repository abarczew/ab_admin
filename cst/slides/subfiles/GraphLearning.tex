\section{Graph Learning}
\begingroup
\setbeamertemplate{navigation symbols}{}  % remove page number within the group
\begin{frame}[noframenumbering]{}
    \centering
    \vspace{2.5cm}
    \Huge
    \textcolor{myblue}{Part 2 \\ - \\ Graph inference from smooth and bandlimited graph signals}
\end{frame}
\endgroup

\subsection{Background}
\begin{frame}{Background}

%\vspace{0.2cm}
%{\textcolor{myblue}{\textbf{Graph learning}}}
%\begin{itemize}
%\item Given a set of vector data, infer which variables are "linked" with each other
%\item Many different frameworks and regularization (e.g. sparsity of the graph, smoothness)
%\item Examples: Graph-Lasso (Friedman \textit{et al.} \cite{friedman2008sparse}), GSP (Shuman \textit{et al.} \cite{shuman2013emerging})
%\end{itemize}
%\pause
\vspace{0.2cm}
{\textcolor{myblue}{\textbf{Graph Signal Processing (GSP)}}}
%
\begin{itemize}
\item Generalizes signal processing concepts for graph signals (smoothness, Fourier transform, sampling, filtering, etc.)
%\item Examples: smoothness, Fourier transform, sampling, filtering, etc.
\item Temporal signals and images are graph signals with specific graph (cycles and grid)
\item Having access to the graph is a strong assumption: graph learning
\end{itemize}
\pause
%
\vspace{1.5em}
\begin{minipage}{0.49\linewidth}
\vspace{-21pt}
\begin{tcolorbox}[title=Definition (Graph Laplacian),size=title,boxrule=0.2pt]
%Recall that the degree matrix of a graph is the diagonal matrix that contains all the nodes degrees.
The graph Laplacian of a graph $G=(\calV,\calE)$ with weight matrix $W$ and degree matrix $D$ is the matrix $L = D - W$
\end{tcolorbox}
\end{minipage}\hfill
%
\begin{minipage}{0.49\linewidth}
\vspace{0pt}
\begin{tcolorbox}[title=Definition (Graph Fourier Transform),size=title,boxrule=0.2pt]
Let $G = (\calV, \calE)$ and $L = X\Lambda X^\transpose$ be the eigenvalue decomposition of its Laplacian matrix. The Graph Fourier Transform (GFT) of a graph signal $y\in \RR^p$ is given by $$h = X^\transpose y$$
\end{tcolorbox}
\end{minipage}
%
\end{frame}

\subsection{Problem Statement}
\begin{frame}{Problem Statement}

{\textcolor{myblue}{\textbf{Goal:}}} Learn the Laplacian $L$ that best explains the structure of $n$ graph signals $Y=[y^{(1)},\ldots,y^{(n)}]$ of size $N$.
\begin{itemize}
\item Need for structural assumptions that link $L$ to $Y$
\end{itemize}
\pause
{\textcolor{myblue}{\textbf{Assumptions:}}} 
\begin{itemize}
\item $G$ is undirected and has a single connected component
\pause
\item The graph signals are \textbf{smooth} with respect to $G$ i.e.  $y^{(i)\transpose} Ly^{(i)}=\frac{1}{2}\sum w_{kl}(y^{(i)}_k-y^{(i)}_l)^2$ is small
\pause
\item They have a \textbf{bandlimited} spectrum i.e. $\forall i$, $h^{(i)} = X^\transpose y^{(i)}$ has some zero-valued coefficients at same dimensions\\
\pause
$\rightarrow$ Basic assumption of sampling methods (Chen \textit{et al.} 2015 \cite{chen2015sampling}) \\
$\rightarrow$ Different notion of smoothness \\
$\rightarrow$ Also relies to the cluster structure of the graph (Sardellitti \textit{et al.} 2019 \cite{sardellitti2018graph})
\end{itemize}

\begin{figure}
\centering
\renewcommand{\ratio}{0.75}
\begin{subfigure}{.32\textwidth}
  \centering
  \includegraphics[width=\ratio\linewidth]{images/graph_no_bandlimited.pdf}
  \caption{}
\end{subfigure}
\begin{subfigure}{.32\textwidth}
  \centering
  \includegraphics[width=\ratio\linewidth]{images/graph_6_bandlimited}
  \caption{}
\end{subfigure}
\begin{subfigure}{.32\textwidth}
  \centering
  \includegraphics[width=\ratio\linewidth]{images/graph_bandlimited.pdf}
  \caption{}
\end{subfigure}
%
\caption{Three smooth graph signals ($N = 300$) with decreasing bandlimitedness: (a) $150$-sparse, (b) $6$-sparse, (c) $3$-sparse.}
\end{figure}
\end{frame}

\subsection{Optimization}

\begingroup
\begin{frame}{Optimization program}
\begin{equation*}
%\label{eq:main}
\min_{H, X, \Lambda} \lVert Y - X H \rVert^2_F + \alpha\lVert\Lambda^{1/2}H\rVert^2_F + \beta \lVert H \rVert_S \; 
\end{equation*}
%
\begin{equation*}
\text{s.t.} \quad \left\{
\begin{array}{l}
X^\transpose X = I_N, x_{1} = \frac{1}{\sqrt{N}}\boldsymbol{1}_N \;   \quad \hfill (\text{a})  \\
(X \Lambda X^\transpose)_{k, \ell} \leq 0 \quad k \neq \ell \;  \quad \hfill (\text{b})   \\
\Lambda = \text{diag}(0,\lambda_2,\ldots, \lambda_N) \succeq 0 \;  \quad \hfill (\text{c}) \\
\text{tr}(\Lambda) = N \in \RR_*^+ \;  \quad \hfill (\text{d})
\end{array}\right.
\end{equation*}
\begin{itemize}
\setlength\itemsep{1em}
\item Learn $X \Lambda X^\transpose$ instead of $L$
\item $Y$ are assumed to be noisy version of some true graph vectors $XH$
\item $H$ stands for the graph Fourier transform of the true graph vectors 
\end{itemize}
\end{frame}

\begin{frame}[noframenumbering]{Optimization program}
\begin{equation*}
%\label{eq:main}
\min_{H, X, \Lambda} \textcolor{red}{\lVert Y - X H \rVert^2_F} + \alpha\lVert\Lambda^{1/2}H\rVert^2_F + \beta \lVert H \rVert_S \; 
\end{equation*}
%
\begin{equation*}
\text{s.t.} \quad \left\{
\begin{array}{l}
X^\transpose X = I_N, x_{1} = \frac{1}{\sqrt{N}}\boldsymbol{1}_N \;   \quad \hfill (\text{a})  \\
(X \Lambda X^\transpose)_{k, \ell} \leq 0 \quad k \neq \ell \;  \quad \hfill (\text{b})   \\
\Lambda = \text{diag}(0,\lambda_2,\ldots, \lambda_N) \succeq 0 \;  \quad \hfill (\text{c}) \\
\text{tr}(\Lambda) = N \in \RR_*^+ \;  \quad \hfill (\text{d})
\end{array}\right.
\end{equation*}
\begin{itemize}
\setlength\itemsep{1em}
\item $\textcolor{red}{\lVert Y - X H \rVert^2_F}$ stands for the reconstruction error %between $Y$ and the inverse Fourier transform of the vectors in $H$
\item $\lVert\Lambda^{1/2}H\rVert^2_F$ controls the smoothness of the approximation $XH$
\item $\lVert H \rVert_S=\lVert H \rVert_{2,0}$ or $\lVert H \rVert_{2,1}$ enforces the GFT to be $0$ at the same dimensions 
\item $\alpha$ and $\beta$ are positive hyperparameters 
\end{itemize}
\end{frame}

\begin{frame}[noframenumbering]{Optimization program}
\begin{equation*}
%\label{eq:main}
\min_{H, X, \Lambda} \lVert Y - X H \rVert^2_F + \alpha\textcolor{red}{\lVert\Lambda^{1/2}H\rVert^2_F} + \beta \lVert H \rVert_S \; 
\end{equation*}
%
\begin{equation*}
\text{s.t.} \quad \left\{
\begin{array}{l}
X^\transpose X = I_N, x_{1} = \frac{1}{\sqrt{N}}\boldsymbol{1}_N \;   \quad \hfill (\text{a})  \\
(X \Lambda X^\transpose)_{k, \ell} \leq 0 \quad k \neq \ell \;  \quad \hfill (\text{b})   \\
\Lambda = \text{diag}(0,\lambda_2,\ldots, \lambda_N) \succeq 0 \;  \quad \hfill (\text{c}) \\
\text{tr}(\Lambda) = N \in \RR_*^+ \; , \quad \hfill (\text{d})
\end{array}\right.
\end{equation*}
\begin{itemize}
\setlength\itemsep{1em}
\item $\lVert Y - X H \rVert^2_F$ stands for the reconstruction error %between $Y$ and the inverse Fourier transform of the vectors in $H$
\item $\textcolor{red}{\lVert\Lambda^{1/2}H\rVert^2_F}$ controls the smoothness of the approximation $XH$
\item $\lVert H \rVert_S=\lVert H \rVert_{2,0}$ or $\lVert H \rVert_{2,1}$ enforces the GFT to be $0$ at the same dimensions 
\item $\alpha$ and $\beta$ are positive hyperparameters 
\end{itemize}
\end{frame}



\begin{frame}[noframenumbering]{Optimization program}
\begin{equation*}
%\label{eq:main}
\min_{H, X, \Lambda} \lVert Y - X H \rVert^2_F + \alpha\lVert\Lambda^{1/2}H\rVert^2_F + \beta \textcolor{red}{\lVert H \rVert_S} \; 
\end{equation*}
%
\begin{equation*}
\text{s.t.} \quad \left\{
\begin{array}{l}
X^\transpose X = I_N, x_{1} = \frac{1}{\sqrt{N}}\boldsymbol{1}_N \;   \quad \hfill (\text{a})  \\
(X \Lambda X^\transpose)_{k, \ell} \leq 0 \quad k \neq \ell \;  \quad \hfill (\text{b})   \\
\Lambda = \text{diag}(0,\lambda_2,\ldots, \lambda_N) \succeq 0 \;  \quad \hfill (\text{c}) \\
\text{tr}(\Lambda) = N \in \RR_*^+ \;  \quad \hfill (\text{d})
\end{array}\right.
\end{equation*}
\begin{itemize}
\setlength\itemsep{1em}
\item $\lVert Y - X H \rVert^2_F$ stands for the reconstruction error %between $Y$ and the inverse Fourier transform of the vectors in $H$
\item $\lVert\Lambda^{1/2}H\rVert^2_F$ controls the smoothness of the approximation $XH$
\item $\textcolor{red}{\lVert H \rVert_S}=\lVert H \rVert_{2,0}$ or $\lVert H \rVert_{2,1}$ enforces the GFT to be $0$ at the same dimensions 
\item $\alpha$ and $\beta$ are positive hyperparameters  that controls smoothness and bandlimitedness
\end{itemize}
\end{frame}





\begin{frame}[noframenumbering]{Optimization program}
\begin{equation*}
%\label{eq:main}
\min_{H, X, \Lambda} \lVert Y - X H \rVert^2_F + \alpha\lVert\Lambda^{1/2}H\rVert^2_F + \beta \lVert H \rVert_S \; 
\end{equation*}
%
\begin{equation*}
\text{s.t.} \quad \left\{
\begin{array}{l}
X^\transpose X = I_N, x_{1} = \frac{1}{\sqrt{N}}\boldsymbol{1}_N \;   \quad \hfill \textcolor{red}{(\text{a})}  \\
(X \Lambda X^\transpose)_{k, \ell} \leq 0 \quad k \neq \ell \;  \quad \hfill \textcolor{red}{(\text{b})}   \\
\Lambda = \text{diag}(0,\lambda_2,\ldots, \lambda_N) \succeq 0 \;  \quad \hfill \textcolor{red}{(\text{c})} \\
\text{tr}(\Lambda) = N \in \RR_*^+ \;  \quad \hfill (\text{d})
\end{array}\right.
\end{equation*}
\begin{itemize}
\setlength\itemsep{1em}
\item $\lVert Y - X H \rVert^2_F$ stands for the reconstruction error %between $Y$ and the inverse Fourier transform of the vectors in $H$
\item $\lVert\Lambda^{1/2}H\rVert^2_F$ controls the smoothness of the approximation $XH$
\item $\lVert H \rVert_S=\lVert H \rVert_{2,0}$ or $\lVert H \rVert_{2,1}$ enforces the GFT to be $0$ at the same dimensions 
\item $\alpha$ and $\beta$ are positive hyperparameters  that controls smoothness and bandlimitedness
\item \textcolor{red}{(a)}, \textcolor{red}{(b)} and \textcolor{red}{(c)} ensure $X \Lambda X^\transpose$ to be a Laplacian
\item (d) makes sure the graph has edges
\end{itemize}
\end{frame}





\begin{frame}[noframenumbering]{Optimization program}
\begin{equation*}
%\label{eq:main}
\min_{H, X, \Lambda} \lVert Y - X H \rVert^2_F + \alpha\lVert\Lambda^{1/2}H\rVert^2_F + \beta \lVert H \rVert_S \; 
\end{equation*}
%
\begin{equation*}
\text{s.t.} \quad \left\{
\begin{array}{l}
X^\transpose X = I_N, x_{1} = \frac{1}{\sqrt{N}}\boldsymbol{1}_N \;   \quad \hfill (\text{a})  \\
(X \Lambda X^\transpose)_{k, \ell} \leq 0 \quad k \neq \ell \;  \quad \hfill (\text{b})   \\
\Lambda = \text{diag}(0,\lambda_2,\ldots, \lambda_N) \succeq 0 \;  \quad \hfill (\text{c}) \\
\text{tr}(\Lambda) = N \in \RR_*^+ \;  \quad \hfill \textcolor{red}{(\text{d})}
\end{array}\right.
\end{equation*}
\begin{itemize}
\setlength\itemsep{1em}
\item $\lVert Y - X H \rVert^2_F$ stands for the reconstruction error %between $Y$ and the inverse Fourier transform of the vectors in $H$
\item $\lVert\Lambda^{1/2}H\rVert^2_F$ controls the smoothness of the approximation $XH$
\item $\lVert H \rVert_S=\lVert H \rVert_{2,0}$ or $\lVert H \rVert_{2,1}$ enforces the GFT to be $0$ at the same dimensions 
\item $\alpha$ and $\beta$ are positive hyperparameters  that controls smoothness and bandlimitedness
\item (a), (b) and (c) ensure $X\Lambda X^\transpose$ to be a Laplacian
\item \textcolor{red}{(d)} makes sure the graph has edges
\end{itemize}
\end{frame}
\begingroup




\begin{frame}{Solving the program (overview)}
\begin{itemize}
\setlength\itemsep{1em}
\item Optimization program not convex $+$ very difficult to updates all variables directly \\
$\longrightarrow$ Use block-coordinate descent 
\item Other problem: constraint (b) $(X\Lambda X^\transpose)_{kl}\leq 0$ difficult to handle at the $X$-step \\
$\longrightarrow$ Solution: \textbf{IGL-3SR} and \textbf{FGL-3SR} [Le Bars \textit{et al.}, ICASSP 2019, Humbert \textit{et al.}, JMLR 2021]
\item Both relax (b) and use block-coordinate descent over $X$, $\Lambda$ and $H$ 
\end{itemize}

\vspace{2em}
\begin{equation*}
%\label{eq:main}
\min_{H, X, \Lambda} \lVert Y - X H \rVert^2_F + \alpha\lVert\Lambda^{1/2}H\rVert^2_F + \beta \lVert H \rVert_S \; 
\end{equation*}
%
\begin{equation*}
\text{s.t.} \quad \left\{
\begin{array}{l}
X^\transpose X = I_N, x_{1} = \frac{1}{\sqrt{N}}\boldsymbol{1}_N \;   \quad \hfill (\text{a})  \\
(X \Lambda X^\transpose)_{k, \ell} \leq 0 \quad k \neq \ell \;  \quad \hfill (\text{b})   \\
\Lambda = \text{diag}(0,\lambda_2,\ldots, \lambda_N) \succeq 0 \;  \quad \hfill (\text{c}) \\
\text{tr}(\Lambda) = N \in \RR_*^+ \;  \quad \hfill (\text{d})
\end{array}\right.
\end{equation*}

\end{frame}



\begin{frame}[noframenumbering]{Solving the program (overview)}
\begin{itemize}
\setlength\itemsep{1em}
\item Optimization program not convex $+$ very difficult to updates all variables directly \\
$\longrightarrow$ Use block-coordinate descent 
\item Other problem: constraint (b) $(X\Lambda X^\transpose)_{kl}\leq 0$ difficult to handle at the $X$-step \\
$\longrightarrow$ Solution: \textbf{IGL-3SR} and \textbf{FGL-3SR} [Le Bars \textit{et al.}, ICASSP 2019, Humbert \textit{et al.} JMLR 2021]
\item Both relax (b) and use block-coordinate descent over $X$, $\Lambda$ and $H$ 
\end{itemize}
%\pause
\begin{minipage}{0.55\linewidth}
\vspace{10pt}
\begin{tcolorbox}[title=IGL-3SR,size=title,boxrule=0.2pt]
Relaxation: use a log-barrier function to put (b) in the objective\\

\textcolor{ForestGreen}{\textbf{+} Each sub-problem is solvable using known techniques} \\
\textcolor{ForestGreen}{\textbf{+} Decrease at each step and stays in the constraint set} \\
\textcolor{ForestGreen}{\textbf{+} Iterates are ensured to converge} \\
\textcolor{Red}{\textbf{-} High complexity} \\
\end{tcolorbox}
\end{minipage}\hfill
%
\begin{minipage}{0.44\linewidth}
\vspace{10pt}
\begin{tcolorbox}[title=FGL-3SR,size=title,boxrule=0.2pt]
Relaxation: get rid of (b), only at the $X$-step\\

\textcolor{ForestGreen}{\textbf{+} Lower complexity} \\
\textcolor{ForestGreen}{\textbf{+} 2/3 steps has closed-form} \\
\textcolor{ForestGreen}{\textbf{+} Returns a Laplacian even with the relaxation} \\
\textcolor{Red}{\textbf{-} Objective function value can increase} \\
\end{tcolorbox}
\end{minipage}
\end{frame}





%\begin{frame}{FGL-3SR}
%\begin{tcolorbox}[title=$H$-step,size=title,boxrule=0.2pt]
%\begin{equation*}
%\min_{H} \lVert Y - X H \rVert^2_F + \alpha \lVert \Lambda^{1/2} H \lVert_F^2 + \beta \lVert H \rVert_{S} \;
%\end{equation*}
%\begin{itemize}
%\item No constraint
%\item Equivalent to multiple sparse linear regression problems
%\item Closed-form solutions
%\item $\lVert \cdot \rVert_{S}=\lVert \cdot \rVert_{2,0}$: hard-thresholding
%\item $\lVert \cdot \rVert_{S}=\lVert \cdot \rVert_{2,1}$: solf-thresholding
%\end{itemize}
%\end{tcolorbox}
%\end{frame}
%
%\begin{frame}[noframenumbering]{FGL-3SR}
%\begin{tcolorbox}[title=$X$-step,size=title,boxrule=0.2pt]
%\begin{equation*}
%\min_{X} \lVert Y - X H \rVert^2_F \qquad \text{s.t.} \quad X^\transpose X = I_N, \ x_{1} = \frac{1}{\sqrt{N}}\boldsymbol{1}_N \qquad  (\text{a})\; 
%\end{equation*}
%\begin{itemize}
%\item (b) is out
%\item Non-convex but has a closed-form:
%\end{itemize}
%	\begin{equation*}
%	X^{(t+1)} = X^{(t)}\begin{bmatrix}
%	1 & \boldsymbol{0}^{\transpose}_{N-1} \\
%	\boldsymbol{0}_{N-1} & PQ^\transpose
%	\end{bmatrix} \; ,
%	\end{equation*}
%where the columns in $P$ and $Q$ are the left- and right-singular vectors of $(X^{(t+1)\transpose} Y H^\transpose)_{2:,2:}$
%\end{tcolorbox}
%\end{frame}
%
%\begin{frame}[noframenumbering]{FGL-3SR}
%\begin{tcolorbox}[title=$\Lambda$-step,size=title,boxrule=0.2pt]
%\begin{equation*}
%\label{eq:linear_prog}
%\min_{\Lambda}\, \alpha\, \underbrace{\text{tr}(HH^\transpose\Lambda)}_{\lVert \Lambda^{1/2}H \rVert^2_F} \qquad \text{s.t.} \quad \left\{
%\begin{array}{l}
%(X \Lambda X^\transpose)_{i,j} \leq 0 \quad i \neq j \; , \quad \hfill (\text{b}) \\
%\Lambda = \text{diag}(0,\lambda_2,\ldots, \lambda_N) \succeq 0 \; , \quad \hfill (\text{c}) \\
%\text{tr}(\Lambda) = N \in \RR_*^+ \; , \quad \hfill (\text{d}) 
%\end{array}\right.
%\end{equation*}
%%
%\begin{itemize}
%\item (b) is back
%\item Linear program: can be solved via solvers
%\item Property: for all $X$ that satisfies (a), there exist $\Lambda$ that satisfies (b), (c) and (d) \\
%$\longrightarrow$ Need to finish by this step
%\end{itemize}
%\end{tcolorbox}
%\end{frame}





\subsection{Experiments}

\begin{frame}{Synthetic data}
\vspace{2em}
\begin{itemize}
\setlength\itemsep{1em}
\item Large simulation study in [Le Bars \textit{et al.}, ICASSP 2019, Humbert \textit{et al.}, JMLR 2021]
%\item $N=20$, $50$ and $100$, $n=1000$
\item True graphs: Random Geometric or Erdös-Renyi
\item $Y$ sampled via factor analysis model
\item Comparison with two GSP baselines:\\
$\rightarrow$ GL-SigRep (Dong \textit{et al.} 2016 \cite{dong2016learning}): Only smoothness \\
$\rightarrow$ ESA-GL (Sardellitti \textit{et al.} 2019 \cite{sardellitti2018graph}): Bandlimitedness 

\end{itemize}
\vspace{1.5em}
\pause
{\textcolor{myblue}{\textbf{Results and conclusion}}}
\begin{itemize}
\setlength\itemsep{1em}
\item IGL-3SR outperforms baselines and FGL-3SR in terms of true graph recovery
\item It is very slow, not practical for $\gtrsim 20$ nodes
\item FGL-3SR is a good compromise between graph recovery and time before convergence
\end{itemize}
\end{frame}

%\begin{frame}{Synthetic data}
%\begin{minipage}[t]{0.48\linewidth}
%\vspace{10pt}
%\centering
%{\textcolor{myblue}{\textbf{Sampling process}}}
%\vspace{1em}
%\begin{itemize}
%\setlength\itemsep{1em}
%\item $N=20$, $50$ and $100$, $n=1000$
%\item Random graph $G$ with $L=X \Lambda X^\transpose$ \\
%$\rightarrow$ Random Geometric (RG) or Erdös-Renyi (ER)
%\item $y=Xh+\varepsilon$ (Factor analysis)
%\item $h\sim\mathcal{N}(0,\Lambda^\dagger)$ and $\varepsilon\sim\mathcal{N}(0,\sigma^2I)$
%\item Half the spectrum of $h$ is zero
%\end{itemize}
%\end{minipage}\hfill
%\begin{minipage}[t]{0.48\linewidth}
%\vspace{10pt}
%\centering
%{\textcolor{myblue}{\textbf{Settings and baselines}}}
%\vspace{1em}
%\begin{itemize}
%\setlength\itemsep{1em}
%\item $\lVert \cdot \rVert_{S}=\lVert \cdot \rVert_{2,1}$
%\item Metrics: $F_1$-score and correlation $\rho(L,\hat{L})$ 
%\item Grid search for $\alpha$ and $\beta$, maximizing $F_1$
%\item Baselines (GSP framework):\\
%$\rightarrow$ GL-SigRep \cite{dong2016learning}: Only smoothness \\
%$\rightarrow$ ESA-GL \cite{sardellitti2018graph}: Bandlimitedness 
%\end{itemize}
%\end{minipage}
%\end{frame}
%
%
%\begin{frame}{Synthetic data - Results}
%\begin{table}%[t]
%	\centering
%	\ra{1.3}
%	\begin{adjustbox}{max width=\textwidth}
%		\begin{tabular}{@{}c|c||cccc|cccc@{}} \toprule
%			&& \multicolumn{4}{c}{\textbf{\emph{RG graph model}}}
%			& \multicolumn{4}{c}{\textbf{\emph{ER graph model}}} \\
%			$N$ & \textbf{Metrics} & \textbf{IGL-3SR} & \textbf{FGL-3SR} & \textbf{ESA-GL}& \textbf{GL-SigRep} & \textbf{IGL-3SR} & \textbf{FGL-3SR} & \textbf{ESA-GL} & \textbf{GL-SigRep} \\
%			%& & \method{(Sardelitti et.al)} & \method{(Dong et.al)} &  & \method{(Sardelitti et.al)} & \method{(Dong et.al)} \\
%			\midrule \midrule
%			%$ {N = 20, n=1000}$ & & & & & & & & \\
%			%\parbox[t]{2mm}{\multirow{5}{*}{\rotatebox[origin=c]{90}{N = 20}}}
%			\parbox[t]{5mm}{\multirow{3}{*}{20}}
%			
%			& $F_1$-measure &$\mathbf{0.97 \,(\pm 0.03)}$ & $ \mathbf{0.97 \,(\pm 0.03)}$ & $0.93 \,(\pm 0.03)$ & $0.95 \,(\pm 0.04)$ & $\mathbf{0.94 \,(\pm 0.03)}$ &$0.82 \,(\pm 0.07)$ & $ \mathbf{0.94 \,(\pm 0.04)}$ & $0.78 \,(\pm 0.07)$ \\
%			
%			& $\rho(L, \hat{L})$ & $\mathbf{0.94 \,(\pm 0.05)} $ & $ {0.90 \,(\pm 0.03)}$ & $0.92 \,(\pm 0.05)$& $0.79 \,(\pm 0.04)$ & $\mathbf{0.92 \,(\pm 0.03)}$ & $0.73 \,(\pm 0.06)$ & $ {0.90 \,(\pm 0.04)}$ & $0.20 \,(\pm 0.07)$\\
%		
%			& Time & $< 1${min} & $< 10${s} & $\mathbf{ {< 5} }$\textbf{s}& $\mathbf{ {< 5} }$\textbf{s}& $< 1${min} & $< 10${s} & $\mathbf{ {< 5} }$\textbf{s}& $\mathbf{ {< 5} }$\textbf{s}\\
%			
%			\midrule
%			%& $ {N = 50, n=1000}$ & & & & & & & &\\
%            %\parbox[t]{2mm}{\multirow{5}{*}{\rotatebox[origin=c]{90}{N = 50}}}
%            \parbox[t]{5mm}{\multirow{3}{*}{50}}
%
%			& $F_1$-measure & $\mathbf{0.90 \,(\pm 0.01)}$ & $0.81 \,(\pm 0.02)$ & $ {0.87 \,(\pm 0.04)}$ & $0.75 \,(\pm 0.01)$ & $0.81 \,(\pm 0.02)$ & $0.76 \,(\pm 0.03)$& $ \mathbf{0.84 \,(\pm 0.02)}$ & $0.61 \,(\pm 0.03)$\\
%
%			& $\rho(L, \hat{L})$ & $\mathbf{0.86 \,(\pm 0.02)}$ & $0.74 \,(\pm 0.03)$ & $ {0.83 \,(\pm 0.03)}$ & $0.55 \,(\pm 0.02)$ & $0.78 \,(\pm 0.03)$ & $0.73 \,(\pm 0.02)$ & $ \mathbf{0.82 \,(\pm 0.06)}$ & $0.06 \,(\pm 0.01)$\\
%			
%			& Time & $ {< 17}${mins} & $\mathbf{ {< 40} }$\textbf{s} & $< 60$s & $\mathbf{ {< 40} }$\textbf{s} & $ {< 17}${mins} & $\mathbf{ {< 40} }$\textbf{s} & $< 60$s & $\mathbf{ {< 40} }$\textbf{s}\\
%			
%			\midrule
%			%$ {N = 100, n=1000}$ & & & & & & & &\\
%	        %\parbox[t]{2mm}{\multirow{5}{*}{\rotatebox[origin=c]{90}{N = 100}}}
%	        \parbox[t]{5mm}{\multirow{3}{*}{100}}
%	
%			& $F_1$-measure & $\mathbf{0.73  \,(\pm 0.03)}$ & $ {0.64 \,(\pm 0.01)}$ & ${0.70} \,(\pm 0.01)$ & -- & $\mathbf{0.62 \,(\pm 0.01)}$ & $ {0.59 \,(\pm 0.02)}$ & $0.59 \,(\pm 0.02)$ & -- \\
%			
%			& $\rho(L, \hat{L})$ & $\mathbf{0.61  \,(\pm 0.04)}$ & $ {0.48 \,(\pm 0.01)}$ & $0.60 \,(\pm 0.03)$ & -- & $0.55 \,(\pm 0.02)$ & $ {0.51 \,(\pm 0.022)}$ & $\mathbf{0.64 \,(\pm 0.02)}$ & -- \\
%
%			& Time & $ {< 50}${mins} & $\mathbf{ {< 2} }$\textbf{mins} & ${< 4}${mins} & -- & ${< 50}${mins} &$\mathbf{ {< 2} }$\textbf{mins} & ${< 4}${mins} & -- \\
%
%			\bottomrule
%		\end{tabular}
%	\end{adjustbox}
%\end{table}
%%
%\begin{itemize}
%\item IGL-3SR outperforms FGL-3SR and have similar performance than ESA-GL
%\item IGL-3SR very slow
%\item FGL-3SR remains competitive while having faster convergence
%\item Baselines begin to slow down (or crash) with $N\geq 50$
%\end{itemize}
%\end{frame}

\begin{frame}[noframenumbering]{Synthetic data - Results}
\begin{figure}
\centering
\includegraphics[width=0.8\linewidth]{images/time_computation.pdf}
\end{figure}
\end{frame}




\begin{frame}{A real-world illustration}
%
\begin{minipage}[t]{0.48\linewidth}
\begin{itemize}
\item Temperature data in Brittany (Chepuri \textit{et al.} 2017 \cite{chepuri2017learning})
\item $N=32$ weather station
\item spectral clustering to ascess the quality
\end{itemize}
\end{minipage} \hfill
%
\begin{minipage}[t]{0.48\linewidth}
\begin{itemize}
\item $n=744$ measurements
\item $\alpha=10^{-4}$, $\beta$ s.t $2$-bandlimited
\end{itemize}
\end{minipage}
%
\vspace{1em}
\begin{figure}
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.9\linewidth]{images/britanny_result_FGL3SR_inkscape}
  \caption{A measurement example and the learned graph.% with node colors to be the average graph signal.
  }
  %\label{fig:meteo_learnFGL}
\end{subfigure}%
 %\vspace{1em}
\begin{subfigure}{.5\textwidth}
  %\centering
  \hspace{0.7em}
  \includegraphics[width=0.79\linewidth]{images/britanny_result_FGL3SR_class_inkscape}
  \caption{Spectral clustering with the learned graph.}
  %\label{fig:meteo_kmean}
\end{subfigure}%
\end{figure}
\begin{itemize}
\item Coherent with the spatial distribution. Splits the north from the south of Brittany
\end{itemize}
\end{frame}


%\subsection*{}
%\begin{frame}{Conclusion}
%\vspace{2em}
%\begin{itemize}
%\setlength\itemsep{1.5em}
%\item First real combination of smooth and bandlimited constraints
%\item IGL-3SR works well but very slow \\
%$\longrightarrow$ Good for a small number of nodes ($\simeq 20$)
%\item FGL-3SR is a good compromise 
%\item Extensive application study in [Humbert \textit{et al.} 2019] + hyperparameter selection study
%\item Does it work at Sigfox? \\
%$\longrightarrow$ Ok but not really adapted
%\end{itemize}
%\end{frame}