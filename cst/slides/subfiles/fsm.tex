\subsection{FSM}\label{subsec:fsm}
\begingroup
\begin{frame}{Repeated querying of hospital unit occupancy\\ modeling patient flows as Finite State Machine}
\begin{minipage}[t]{0.48\linewidth}
    \centering\textbf{Problem statement}\\
    \begin{itemize}
      \item Hospital unit needs to report the number of patients in all of its services
      \item We define each patient as a FSM (see below) with a maximum length of stay $T_{max}$
      \item How to publish these reports publically while preserving privacy?
    \end{itemize}
    \centering
    \begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=1.7cm,semithick]
    \tikzstyle{every state}=[fill=red,draw=none,text=white]

    \node[initial,state] (A)                    {reception};
    \node[state]         (D) [above right of=A] {surgery};
    \node[state]         (E) [below right of=A] {examination};
    \node[state]         (B) [right of=D]       {ward};
    \node[state]         (C) [right of=E]       {IC};
    \node[state]         (F) [above right of=C] {exit};

    \path (A) edge              node {} (E)
    edge              node {} (D)
    edge [loop above] node {} (A)
    edge              node {} (F)
    (B) edge              node {} (E)
    edge [loop above] node {} (B)
    edge              node {} (F)
    (C) edge              node {} (B)
    edge [loop below] node {} (C)
    edge              node {} (F)
    (D) edge              node {} (B)
    edge [loop above] node {} (D)
    edge              node {} (F)
    (E) edge              node {} (C)
    edge              node {} (D)
    edge [loop left]  node {} (E)
    edge              node {} (F);
    \end{tikzpicture}
    \pause
\end{minipage}\hfill
\begin{minipage}[t]{0.47\linewidth}
        \centering\textbf{Challenges}\\
        \begin{itemize}
           \item Sensitive data requiring privacy guarantees
           \item Multiple querying
           \item Infinite time of analysis but fixed duration of process
           \item Complex underlying processes
        \end{itemize}
\end{minipage}

\end{frame}
\endgroup

\begingroup
\begin{frame}[t]{We leverage binary mecahnism to count privately with underlying process}
\begin{minipage}[t]{0.48\linewidth}
  \vspace{0pt}
  \begin{tcolorbox}[title= Algorithm (BinMech) \citep{dwork2010differential},size=title,boxrule=0.2pt]
  \scalebox{0.9}{
     \begin{algorithm}[H]
         \SetKwInOut{Input}{Input}
         \SetKwInOut{Output}{Output}
         \Input{$\epsilon$, $T_{max}$, a stream
         $\sigma_{in} \in \{-1, 0, 1\}^{\mathbb{N}}$ .}
         \Output{At each time step $t$, output estimate $\mathcal{B}(t)$.}
         $\epsilon' \leftarrow \epsilon / \log(T_{max})$; Each $\hat{\alpha_{i}}$ is (implicitly) initialized to $0$; $count \leftarrow \mathrm{Lap}(\frac{1}{\epsilon'})$\;
         \For{$t \leftarrow 1$ \KwTo $\infty$}{
           $count \leftarrow count + \sigma(t)$\;
           Let $i := \min\{j: \mathrm{Bin}_j(t) \neq 0\}$\;
           \For{$j \leftarrow 0$ \KwTo $i-1$}{
             $\hat{\alpha_{j}} \leftarrow 0$
           }
           $\hat{\alpha_{i}} \leftarrow \mathrm{Lap}(\frac{1}{\epsilon'})$\;
           return the estimate at time $t$:
           $\mathcal{B}(t) = count + \sum_{j: \mathrm{Bin}_j(t)=1} \hat{\alpha_{j}}$
         }
     \end{algorithm}
    }
  \end{tcolorbox}
\end{minipage}\hfill
\pause
\begin{minipage}[t]{0.48\linewidth}
  \vspace{0pt}
  \begin{tcolorbox}[title= Algorithm (BinMechProcess),size=title,boxrule=0.2pt]
  \scalebox{0.9}{
 \begin{algorithm}[H]
     \SetKwInOut{Input}{Input}
     \SetKwInOut{Output}{Output}

     \Input{$\epsilon$, $T_{max}$, a stream
     $\sigma_{in} \in \{-1, 0, 1\}^{\mathbb{N}}$.}
     \Output{At each time step $t$, output estimate $\mathcal{B}(t)$.}
     \For{$t \leftarrow 1$ \KwTo $T_{max}$}{
       $\mathcal{B}(t) = \textsc{BinMech}(t, \sigma,\epsilon, T_{max})$
     }
     Let $m := \max\{i: \mathrm{Bin}_i(T_{max}) \neq 0\}$;
     Let $t_{bin} := T_{max}$\;
     \While{$t < \infty$}{
       Let $\alpha_{old} := \hat{\alpha_m}$; \Comment{we store old noise}
       $\hat{\alpha_m} \leftarrow 0$\;
       \For{$t_{sub} \leftarrow 1$ \KwTo $2^m$}{
           $\mathcal{B}(t) = \textsc{BinMech}(t_{bin}, \sigma,\epsilon, 2^m) + \alpha_{old}$\;
           $t_{bin} \leftarrow t_{bin} + 1$\;
           $t \leftarrow t + 1$\;
       }
       $t_{bin} \leftarrow t_{bin} - 2^m$ \Comment{we reset index}
     }
 \end{algorithm}
}
  \end{tcolorbox}
\end{minipage}

\end{frame}
\endgroup

\begingroup
\begin{frame}[t]{Our algorithm works on synthetic data}
\begin{figure}
\hskip -0.2in
\centering
\includegraphics[width=0.5\columnwidth]{figures/dp_counter_boundaries.png}
\caption{MSE between DP and true count in simulated hospital units depending on $T_{max}$}
\end{figure}
  \begin{itemize}
  \item We have created synthetic data that replicate streams of patients in hospital services
  \item Error grows with smaller $\epsilon$
  \item Error increases logarithmically with $T_{max}$
  \item Error is close to the theoretical one
\end{itemize}

\end{frame}
\endgroup

\begin{frame}[c]{Future work}
\begin{itemize}
     \item We can \textcolor{red}{generalize on the length of the stay} with $T_{max}$ follows a poisson distribution
     \item We need to build a \textcolor{red}{more robust model} so it can cope with edge cases eg patient move units several times within the same day etc
     \item We only focus on counting but we should be able to answer \textcolor{red}{more complex statistical queries}
   \end{itemize}
\end{frame}
