\beamer@sectionintoc {1}{Motivations}{2}{0}{1}
\beamer@sectionintoc {2}{Thesis progress}{6}{0}{2}
\beamer@subsectionintoc {2}{1}{FSM}{7}{0}{2}
\beamer@subsectionintoc {2}{2}{ECDF}{13}{0}{2}
\beamer@subsectionintoc {2}{3}{Empirical DP optimum}{24}{0}{2}
\beamer@sectionintoc {3}{Roadmap}{29}{0}{3}
\beamer@sectionintoc {4}{Doctoral training}{31}{0}{4}
\beamer@sectionintoc {5}{Professional project}{35}{0}{5}
